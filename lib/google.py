# -*- coding: utf-8 -*-
import requests


def get_google_verification_data(access_token):
    google_auth_url = (
        'https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=%s'
    )
    request_url = google_auth_url % access_token

    response = None
    missed_connections = 0
    while response is None:
        try:
            response = requests.get(request_url)
        except requests.exceptions.ConnectionError:
            missed_connections += 1
            if missed_connections > 3:
                return {}

    if not response.ok:
        return {}

    response_dict = response.json()

    return {
        "verified": response_dict.get("verified_email"),
        "email": response_dict.get('email')
    }

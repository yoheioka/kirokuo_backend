# -*- coding: utf-8 -*-
import arrow


def get_date_in_japan():
    date = arrow.now('Asia/Tokyo')
    date_string = date.format('YYYY-MM-DD')
    return arrow.get(date_string)

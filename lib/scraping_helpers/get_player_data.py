# -*- coding: utf-8 -*-
import urllib2
from bs4 import BeautifulSoup
from lib.models.player import Player
from short_names import ID_TO_SHORT


YAHOO_TEAM_PLAYERS_URL = 'http://baseball.yahoo.co.jp/npb/teams'

PLAYER_POSITION_MAP_J_E = {
    u'内野手': 'IF',
    u'外野手': 'OF',
    u'投手': 'P',
    u'捕手': 'C'
}

HANDED = {
    u'左': 'LH',
    u'右': 'RH',
    u'両': 'SH'
}


def get_player_ids():
    player_ids = []
    for i in [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 376]:
        # getting pitcher ids
        url = '%s/%s/memberlist?type=p' % (YAHOO_TEAM_PLAYERS_URL, i)
        page = urllib2.urlopen(url)
        soup = BeautifulSoup(page.read())
        pitchers = soup.find(id='tm_pitt').find_all('td', 'yjM')
        for pitcher in pitchers:
            player_ids.append(int(
                pitcher.find('a').get('href').split(
                    'player/')[1].replace('/', '')
            ))

        # getting batter ids
        url = '%s/%s/memberlist?type=b' % (YAHOO_TEAM_PLAYERS_URL, i)
        page = urllib2.urlopen(url)
        soup = BeautifulSoup(page.read())
        batters = soup.find(id='tm_batt').find_all('td', 'yjM')
        for batter in batters:
            player_ids.append(int(
                batter.find('a').get('href').split(
                    'player/')[1].replace('/', '')
            ))
    return player_ids


def get_and_save_player_data(player_id):
    url = 'http://baseball.yahoo.co.jp/npb/player/%s/' % player_id
    try:
        page = urllib2.urlopen(url)
        soup = BeautifulSoup(page.read())
    except urllib2.HTTPError:
        print 'ERROR scraping player: %s' % player_id
        return None
    profile = soup.find(id='profile')
    profile_string = profile.find('h1').text
    name = profile_string.split(u'（')[0].replace(u'　', '')
    number_string = profile.find('em').text
    position = PLAYER_POSITION_MAP_J_E[profile.find('span', 'position').text]
    team = soup.find('span', 'NpbTeamLogoTop').get('class')[1]
    # TODO img not working
    # img = profile.find('img').get('src')
    img = ''
    for el in profile.find_all('td'):
        text = el.text
        if u'投げ' == text[1:3] and u'打ち' == text[4:6]:
            text = text.replace(u'投げ', '').replace(u'打ち', '')
            throwing = HANDED[text[0]]
            batting = HANDED[text[1]]
    short_name = ID_TO_SHORT.get(player_id, '')
    if not short_name:
        print '%s (%s) missing short name ' % (name, player_id)
    args = {
        'id': player_id,
        'num': number_string,
        'name': name,
        'position': position,
        'batting': batting,
        'throwing': throwing,
        'img': img,
        'team': team,
        'short_name': short_name
    }
    return Player(**args)

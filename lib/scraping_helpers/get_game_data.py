# -*- coding: utf-8 -*-
import arrow
import urllib2
from bs4 import BeautifulSoup
from lib.models.game import Game, GameStatuses
from lib.registry import get_registry
from lib.models.batter_by_day import BatterByDay


class ScrapeGameHelper(object):

    def __init__(self):
        self.db = get_registry()['DB']
        self.game_repo = get_registry()['GAME_REPO']
        self.player_repo = get_registry()['PLAYER_REPO']

    YAHOO_SCHEDULE_URL = 'http://baseball.yahoo.co.jp/npb/schedule/?date=%s'
    YAHOO_BB_MONTH_URL = 'http://baseball.yahoo.co.jp/npb/game'
    SEASON_START = arrow.get('2016-03-25')
    SEASON_END = arrow.get('2016-09-28')
    ALL_STAR = [
        arrow.get('2016-07-15'),
        arrow.get('2016-07-16')
    ]

    POSITION_MAP_J_E = {
        u'左': 'LF',
        u'中': 'CF',
        u'右': 'RF',
        u'一': 'F',
        u'二': 'S',
        u'遊': 'SS',
        u'三': 'T',
        u'捕': 'C',
        u'投': 'P',
        u'走': 'PR',
        u'指': 'DH',
        u'打': 'PH'
    }

    PLAYER_POSITION_MAP_J_E = {
        u'内野手': 'IF',
        u'外野手': 'OF',
        u'投手': 'P',
        u'捕手': 'C'
    }

    def get_dates(self, all_dates=False):
        start = max(arrow.utcnow().floor('day'), self.SEASON_START)
        if all_dates:
            start = self.SEASON_START
        return arrow.Arrow.range('day', start, self.SEASON_END)

    def scrape_game_data(self, all_dates=False):
        games = []
        for d in self.get_dates(all_dates=all_dates):
            if d in self.ALL_STAR:
                continue
            try:
                games.extend(self.scrape_games_for_date(d))
            except:
                print "ERROR SCRAPING %s" % d
        return(games)

    def scrape_games_for_date(self, date):
        url = self.YAHOO_SCHEDULE_URL % date.format('YYYYMMDD')
        page = urllib2.urlopen(url)
        soup = BeautifulSoup(page.read())
        game_groups = soup.find_all('div', 'NpbScoreBg')
        games = []
        game_num = 1
        for game_group in game_groups:
            scraped_games = game_group.find_all('table', 'teams')
            for scraped_game in scraped_games:
                teams = scraped_game.find_all('div', 'pn')
                away = teams[0].get('class')[1]
                home = teams[1].get('class')[1]
                start_time_string = scraped_game.find('em').text
                japan_start_time_string = '%s %s+09:00' % (
                    date.format('YYYY-MM-DD'), start_time_string)
                start_time = arrow.get(japan_start_time_string).timestamp

                args = {
                    'year': date.year,
                    'date': date.timestamp,
                    'game_number': game_num,
                    'away': away,
                    'home': home,
                    'start_time': start_time
                }

                games.append(Game(**args))
                game_num += 1
        return games

    def scrape_team_batting_stats(self, stats, team, opponent, is_away, date):
        for player_row in stats:
            p_data = player_row.find_all('td')
            position = p_data[0].text

            # check for cancelled games
            if not position:
                continue
            starting = position[0] == '('
            position = position.strip('(').strip(')')
            position = set([self.POSITION_MAP_J_E[p] for p in position])
            player_id = int(p_data[1].find('a').get('href').split('id=')[1])
            ab = int(p_data[3].text)
            r = int(p_data[4].text)
            rbi = int(p_data[6].text)
            sb = int(p_data[10].text)
            h1b = h2b = h3b = hr = bb = so = hbp = sh = sf = roe = gdp = 0
            at_bats = [x.contents for x in p_data[13:] if len(x) > 0]
            at_bats = sum(at_bats, [])  # this flattens the list
            at_bats = [at_bat.string for at_bat in at_bats if at_bat.string]

            for at_bat in at_bats:
                if u'安' in at_bat:
                    h1b += 1
                if u'２' in at_bat:
                    h2b += 1
                if u'３' in at_bat:
                    h3b += 1
                if u'本' in at_bat:
                    hr += 1
                if u'四' in at_bat or u'敬' in at_bat:
                    bb += 1
                if u'死' in at_bat:
                    hbp += 1
                if u'振' in at_bat:
                    so += 1
                if u'犠打' in at_bat:
                    sh += 1
                if u'犠飛' in at_bat:
                    sf += 1
                if u'失' in at_bat:
                    roe += 1
                if u'併打' in at_bat:
                    gdp += 1

            args = {
                'id': player_id,
                'year': date.year,
                'date': date.timestamp,
                'team': team,
                'away': is_away,
                'starting': starting,
                'opponent': opponent,
                'position': str(position),  # use eval(string) to decode
                'ab': ab,
                'r': r,
                'h1b': h1b,
                'h2b': h2b,
                'h3b': h3b,
                'hr': hr,
                'rbi': rbi,
                'bb': bb,
                'so': so,
                'hbp': hbp,
                'sh': sh,
                'sf': sf,
                'sb': sb,
                'roe': roe,
                'gdp': gdp
            }

            batter_by_day = BatterByDay(**args)
            self.db.session.merge(batter_by_day)
            self.db.session.commit()

    def get_stats_from_url(self, date, url):
        page = urllib2.urlopen(url)
        soup = BeautifulSoup(page.read())
        away = soup.find(id='tb1')
        home = soup.find(id='tb2')
        if not (away and home):
            return
        away = away.find('th').get('class')[0]
        home = home.find('th').get('class')[0]
        away_batting_stats = soup.find(id='st_battv')
        home_batting_stats = soup.find(id='st_batth')
        if not (away_batting_stats and home_batting_stats):
            return
        away_batting_stats = away_batting_stats.find_all('tr')[1: -1]
        home_batting_stats = home_batting_stats.find_all('tr')[1: -1]

        self.scrape_team_batting_stats(away_batting_stats,
                                       away, home, True, date)
        self.scrape_team_batting_stats(home_batting_stats,
                                       home, away, False, date)

    def get_game_status_from_url(self, url, date):
        page = urllib2.urlopen(url)
        soup = BeautifulSoup(page.read())
        game_card = soup.find(id='gm_match')
        if not game_card:
            return
        home = game_card.find('div', 'column-left').find('a').get('class')[1]
        away = game_card.find('div', 'column-right').find('a').get('class')[1]
        game = self.game_repo.get_game_for_teams(date, home, away)

        status = game_card.find('div', 'column-center').find('em').text

        if u'試合終了' in status:
            status = GameStatuses.COMPLETED
        elif u'中止' in status:
            status = GameStatuses.CANCELLED
            self.player_repo.remove_stats_for_cancelled_game(
                date, home, away
            )
        elif u'回' in status:
            status = GameStatuses.LIVE
        elif u'vs.' in status:
            status = GameStatuses.NOT_STARTED
        else:
            print '### UNKNOW STATUS %s ###' % status
            return
        game.status = status
        self.db.session.merge(game)
        self.db.session.commit()

    def scrape_player_game_data(self, date):
        count = 0
        for number in range(1, 8):
            if not self.game_repo.get_game(date, number):
                continue

            date_game_string = '%s0%s' % (date.format('YYYYMMDD'), number)
            url = '%s/%s/stats' % (self.YAHOO_BB_MONTH_URL, date_game_string)
            try:
                self.get_stats_from_url(date, url)
            except urllib2.HTTPError:
                pass
            # scrape game status
            url = '%s/%s/top' % (self.YAHOO_BB_MONTH_URL, date_game_string)
            try:
                self.get_game_status_from_url(url, date)
            except urllib2.HTTPError:
                pass
            count += 1
        print 'scraped %s games' % count

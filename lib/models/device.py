# -*- coding: utf-8 -*-
from lib.registry import get_registry

db = get_registry()['DB']


class Device(db.Model):

    device_token = db.Column(db.String(255), primary_key=True)
    email = db.Column(db.ForeignKey('user.email'), primary_key=True)
    updated_at = db.Column(db.Integer)

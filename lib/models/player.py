# -*- coding: utf-8 -*-
from lib.registry import get_registry

db = get_registry()['DB']

OTANI = 1200087
PITCHER = 'P'


class Player(db.Model):

    PLAYER_POSITION_MAP_E_J = {
        'IF': u'内野手',
        'OF': u'外野手',
        'P': u'投手',
        'C': u'捕手'
    }

    HANDED_E_J = {
        'LH': u'左',
        'RH': u'右',
        'SH': u'両'
    }

    id = db.Column(db.Integer, primary_key=True)
    num = db.Column(db.String(3))
    team = db.Column(db.String(5))
    name = db.Column(db.String(32))
    position = db.Column(db.String(5))
    batting = db.Column(db.String(2))
    throwing = db.Column(db.String(2))
    img = db.Column(db.String(255))
    short_name = db.Column(db.String(32))

    @property
    def is_batter(self):
        if self.id == OTANI:
            return True
        return self.position != PITCHER

    def to_dict(self, stats={}):
        return {
            'id': self.id,
            'name': self.name,
            'num': self.num,
            'team': self.team,
            'position': self.PLAYER_POSITION_MAP_E_J[self.position],
            'batting': self.HANDED_E_J[self.batting],
            'at_bats': stats.get('at_bats', 0),
            'hits': stats.get('hits', 0)
        }

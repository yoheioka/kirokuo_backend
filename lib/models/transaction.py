# -*- coding: utf-8 -*-
from lib.registry import get_registry

db = get_registry()['DB']


class TransactionType(object):
    JOIN_BONUS = 'join_bonus'
    MEDALS_10 = 'medals_10'
    MEDALS_50 = 'medals_50'
    INSURANCE = 'insurance'
    HIT_BONUS = 'hit_bonus'

    MULTI_HIT_BONUS = 'multi_hit_bonus_%s'

    REASON_IN_JAPANESE = {
        JOIN_BONUS: u'新規登録ボーナス',
        MEDALS_10: u'メダル購入',
        MEDALS_50: u'メダル購入',
        INSURANCE: u'保険適用',
        HIT_BONUS: u'安打ボーナス'
    }

    TRANSACTION_ID_TO_AMOUNT = {
        MEDALS_10: 10,
        MEDALS_50: 50
    }

    def generate_multi_hit_reason(self, streak):
        return self.MULTI_HIT_BONUS % streak

    def get_jp_reason(self, reason):
        if reason in self.REASON_IN_JAPANESE:
            return self.REASON_IN_JAPANESE[reason]

        if reason.startswith('multi_hit_bonus_'):
            return u'%s試合連続安打ボーナス' % reason[-1]


class Transaction(db.Model):
    id = db.Column(db.String(255), primary_key=True, unique=True)
    user_email = db.Column(db.String(255), db.ForeignKey('user.email'))
    date = db.Column(db.Integer)
    amount = db.Column(db.Integer)
    reason = db.Column(db.String(255))
    updated_at = db.Column(db.Integer)

    def to_dict(self):
        return {
            'date': self.date,
            'amount': self.amount,
            'reason': TransactionType().get_jp_reason(self.reason)
        }

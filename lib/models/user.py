# -*- coding: utf-8 -*-
from flask.ext.security import UserMixin
from roles_users import roles_users
from lib.registry import get_registry
from config import Bulbasaur

db = get_registry()['DB']


class User(db.Model, UserMixin):

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    stripped_email = db.Column(db.String(255), unique=True)
    nickname = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.Integer)
    source = db.Column(db.String(255))
    confirmation_code = db.Column(db.String(255))
    confirmation_attempt = db.Column(db.Integer,
                                     default=0,
                                     server_default='0',
                                     nullable=False)
    password_confirmation_code = db.Column(db.String(255))
    password_confirmation_attempt = db.Column(db.Integer,
                                              default=0,
                                              server_default='0',
                                              nullable=False)
    current_balance = db.Column(db.Integer,
                                default=0,
                                server_default='0',
                                nullable=False)
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    accepted_terms = db.Column(db.Integer)

    @property
    def accepted_recent_terms(self):
        if not self.accepted_terms:
            return False
        return self.accepted_terms >= Bulbasaur.Config.MIN_TERMS_REV

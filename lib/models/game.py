# -*- coding: utf-8 -*-
import arrow
from lib.registry import get_registry

db = get_registry()['DB']


class GameStatuses():

    NOT_STARTED = 'not_started'
    COMPLETED = 'completed'
    CANCELLED = 'cancelled'
    LIVE = 'live'


class Game(db.Model):

    year = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Integer, primary_key=True)
    game_number = db.Column(db.Integer, primary_key=True)
    away = db.Column(db.String(5))
    home = db.Column(db.String(5))
    status = db.Column(db.String(32),
                       default=GameStatuses.NOT_STARTED,
                       server_default=GameStatuses.NOT_STARTED,
                       nullable=False)
    away_pitcher = db.Column(db.Integer)
    home_pitcher = db.Column(db.Integer)
    start_time = db.Column(db.Integer)

    @property
    def locked(self):
        now = arrow.utcnow()
        lock_time = arrow.get(self.start_time).replace(minutes=-10)
        return now > lock_time

    def to_dict(self):
        return {
            'year': self.year,
            'date': self.date,
            'game_number': self.game_number,
            'away': self.away,
            'home': self.home,
            'status': self.status,
            'start_time': self.start_time
        }

    @property
    def cancelled(self):
        return self.status == GameStatuses.CANCELLED

# -*- coding: utf-8 -*-
import arrow
from lib.registry import get_registry

db = get_registry()['DB']


class EntryStatus(object):

    PENDING = 'pending'
    INSURED = 'insured'
    SUCCESS = 'success'
    FAILED = 'failed'
    NO_APPEARANCE = 'no_appearance'


class Entry(db.Model):

    year = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), primary_key=True)
    game_number = db.Column(db.Integer)
    game_start = db.Column(db.Integer)
    player_id = db.Column(db.Integer)
    use_insurance = db.Column(db.Boolean)
    entry_at = db.Column(db.Integer)
    result = db.Column(db.String(255))
    finalized = db.Column(db.Boolean)

    @property
    def locked(self):
        now = arrow.utcnow()
        lock_time = arrow.get(self.game_start).replace(minutes=-10)
        return now > lock_time

    @property
    def failed(self):
        return self.result == EntryStatus.FAILED

    @property
    def success(self):
        return self.result == EntryStatus.SUCCESS

    def to_dict(self, streak_count, player, player_stats):
        hits = 0
        at_bats = 0
        if player_stats:
            hits = player_stats.hits
            at_bats = player_stats.ab
        return {
            'date': self.date,
            'player_id': self.player_id,
            'player': player.name,
            'result': self.result,
            'streak': streak_count,
            'result': self.result,
            'hits': hits,
            'at_bats': at_bats,
            'team': player.team
        }

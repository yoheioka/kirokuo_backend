# -*- coding: utf-8 -*-
from lib.registry import get_registry

db = get_registry()['DB']


class Streak(db.Model):

    email = db.Column(db.ForeignKey('user.email'), primary_key=True)
    started_at = db.Column(db.Integer, primary_key=True)
    updated_at = db.Column(db.Integer)
    count = db.Column(db.Integer)
    ended = db.Column(db.Boolean)
    insurance_count = db.Column(db.Integer)

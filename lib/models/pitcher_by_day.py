# -*- coding: utf-8 -*-
from lib.registry import get_registry

db = get_registry()['DB']


class PitcherByDay(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    year = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Integer, primary_key=True)
    team = db.Column(db.String(5))
    away = db.Column(db.Boolean)
    starting = db.Column(db.Boolean)
    opponent = db.Column(db.String(5))
    w = db.Column(db.Integer)
    l = db.Column(db.Integer)
    sv = db.Column(db.Integer)
    gp = db.Column(db.Integer)
    ip_times_three = db.Column(db.Integer)
    bf = db.Column(db.Integer)
    h = db.Column(db.Integer)
    hr = db.Column(db.Integer)
    so = db.Column(db.Integer)
    bb = db.Column(db.Integer)
    r = db.Column(db.Integer)
    er = db.Column(db.Integer)
    cg = db.Column(db.Integer)
    cgso = db.Column(db.Integer)
    nh = db.Column(db.Integer)
    pg = db.Column(db.Integer)

# -*- coding: utf-8 -*-
from lib.registry import get_registry

db = get_registry()['DB']


class BatterByDay(db.Model):

    id = db.Column(db.Integer, primary_key=True)
    year = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Integer, primary_key=True)
    team = db.Column(db.String(5))
    away = db.Column(db.Boolean)
    starting = db.Column(db.Boolean)
    opponent = db.Column(db.String(5))
    position = db.Column(db.String(32))
    ab = db.Column(db.Integer)
    r = db.Column(db.Integer)
    h1b = db.Column(db.Integer)
    h2b = db.Column(db.Integer)
    h3b = db.Column(db.Integer)
    hr = db.Column(db.Integer)
    rbi = db.Column(db.Integer)
    bb = db.Column(db.Integer)
    so = db.Column(db.Integer)
    hbp = db.Column(db.Integer)
    sh = db.Column(db.Integer)
    sf = db.Column(db.Integer)
    sb = db.Column(db.Integer)
    roe = db.Column(db.Integer)
    gdp = db.Column(db.Integer)

    @property
    def hits(self):
        return self.h1b + self.h2b + self.h3b + self.hr

    @property
    def count_appearance(self):
        return self.ab > 0 or self.sf > 0

# -*- coding: utf-8 -*-


class UserDoesntExist(Exception):
    def __init__(self, message='user_doesnt_exist'):
        Exception.__init__(self, message)


class FacebookAccountExists(Exception):
    def __init__(self, message='facebook_account_exists'):
        Exception.__init__(self, message)


class GoogleAccountExists(Exception):
    def __init__(self, message='google_account_exists'):
        Exception.__init__(self, message)


class AccountExists(Exception):
    def __init__(self, message='account_exists'):
        Exception.__init__(self, message)


class AlreadyConfirmed(Exception):
    def __init__(self, message='already_confirmed'):
        Exception.__init__(self, message)


class MissingEmailOrPassword(Exception):
    def __init__(self, message='missing_email_or_password'):
        Exception.__init__(self, message)


class MissingAccessToken(Exception):
    def __init__(self, message='missing_access_token'):
        Exception.__init__(self, message)


class AuthenticationFailed(Exception):
    def __init__(self, message='authentication_failed'):
        Exception.__init__(self, message)


class NotVerified(Exception):
    def __init__(self, message='not_verified'):
        Exception.__init__(self, message)


class NotConfirmed(Exception):
    def __init__(self, message='not_confirmed'):
        Exception.__init__(self, message)


class LoginFailed(Exception):
    def __init__(self, message='login_failed'):
        Exception.__init__(self, message)


class NoEmailAccount(Exception):
    def __init__(self, message='no_email_account'):
        Exception.__init__(self, message)


class InvalidConfirmationCode(Exception):
    def __init__(self, message='invalid_confirmation_code'):
        Exception.__init__(self, message)


class TooManyConfirmationAttempts(Exception):
    def __init__(self, message='too_many_confirmation_attempts'):
        Exception.__init__(self, message)


class PlayerDoesntExist(Exception):
    def __init__(self, message='player_doesnt_exist'):
        Exception.__init__(self, message)


class GameDoesntExist(Exception):
    def __init__(self, message='game_doesnt_exist'):
        Exception.__init__(self, message)


class EntryDoesntExist(Exception):
    def __init__(self, message='entry_doesnt_exist'):
        Exception.__init__(self, message)


class PlayerNotInGame(Exception):
    def __init__(self, message='player_not_in_game'):
        Exception.__init__(self, message)


class GameLocked(Exception):
    def __init__(self, message='game_locked'):
        Exception.__init__(self, message)


class EntryLocked(Exception):
    def __init__(self, message='entry_locked'):
        Exception.__init__(self, message)


class UpdateRequired(Exception):
    def __init__(self, message='update_required'):
        Exception.__init__(self, message)


class GameNotReady(Exception):
    def __init__(self, message='game_not_ready'):
        Exception.__init__(self, message)


class NotEnoughMedals(Exception):
    def __init__(self, message='not_enough_medals'):
        Exception.__init__(self, message)


class InsuranceAlreadyUsed(Exception):
    def __init__(self, message='insurance_already_used'):
        Exception.__init__(self, message)


class MissingNickname(Exception):
    def __init__(self, message='missing_nickname'):
        Exception.__init__(self, message)


class NicknameExists(Exception):
    def __init__(self, message='nickname_exists'):
        Exception.__init__(self, message)


class TermsNotAccepted(Exception):
    def __init__(self, message='terms_not_accepted'):
        Exception.__init__(self, message)

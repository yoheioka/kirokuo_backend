# -*- coding: utf-8 -*-
from config import Bulbasaur
from flask_mail import Message
from registry import get_registry
import boto.sqs
from boto.sqs.message import Message as AmazonMessage
import pickle


class EmailHelper(object):

    def __init__(self):
        self.mail = get_registry()['MAIL']
        self.sqs_conn = boto.sqs.connect_to_region(
            Bulbasaur.Config.AWS_REGION,
            aws_access_key_id=Bulbasaur.Config.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=Bulbasaur.Config.AWS_SECRET_ACCESS_KEY)
        self.q = self.sqs_conn.create_queue('confirmation')

    def queue_email(self, email, confirmation_code, topic):
        m = AmazonMessage()
        data = {
            'sender': Bulbasaur.Config.MAIL_USERNAME,
            'confirmation_code': confirmation_code,
            'topic': topic,
            'email': email
        }
        m.set_body(pickle.dumps(data))
        self.q.write(m)

    def send_confirmation_email(self, sender, email, confirmation_code):
        msg = Message(
            u'⚾ 記録王 ⚾ ようこそ!',
            sender=sender,
            recipients=[email])
        msg.html = u"""
        この度は登録して頂きどうもありがとうございます！<br>

        次の確認コードをアプリで入力してください<br>

        確認コード： %s<br><br>

        記録王
        """ % (confirmation_code)
        self.mail.send(msg)

    def resend_confirmation_email(self, sender, email, confirmation_code):

        msg = Message(
            u'⚾ 記録王 ⚾ ようこそ!',
            sender=sender,
            recipients=[email])
        msg.html = u"""
        いつもご利用いただきありがとうございます！<br>

        次の確認コードをアプリで入力してください<br>

        確認コード： %s<br><br>

        記録王
        """ % (confirmation_code)
        self.mail.send(msg)

    def send_password_confirmation_email(self, sender, email,
                                         confirmation_code):

        msg = Message(
            u'⚾ 記録王 ⚾ パスワードをリセットしてください',
            sender=sender,
            recipients=[email])
        msg.html = u"""
        いつもご利用いただきありがとうございます！<br>

        次の確認コードをアプリで入力してください<br>

        確認コード： %s<br><br>

        記録王
        """ % (confirmation_code)
        self.mail.send(msg)

# -*- coding: utf-8 -*-
import time
import random
import registry

from redis.exceptions import ConnectionError

# don't try to release the lock in the last 25 msec - let it timeout
# (lest we remove a lock we don't own)
LOCK_FUDGE = 0.025

# when the lock is contended, pause for between 1 msec and 50 msec
MIN_SLEEP = 0.001
MAX_SLEEP = 0.05


class Lock(object):

    def __init__(self, client, key, duration=2, timeout=2):
        self.client = client
        self.key = key
        self.duration = duration
        self.timeout = timeout
        self.deadline = None
        self.lockexp = None

    def __enter__(self):
        self.deadline = time.time() + self.timeout
        while time.time() < self.deadline:
            try:
                if self.client.set(self.key, '', nx=True,
                                   px=int(self.duration*1000)):
                    self.lockexp = time.time() + self.duration - LOCK_FUDGE
                    return
                time.sleep(random.uniform(MIN_SLEEP, MAX_SLEEP))
            except ConnectionError:
                raise AcquireLockError('Lock on %s' % self.key)
        raise LockTimeout("Lock on %s timed out after %f sec" % (
            self.key, self.timeout)
        )

    def __exit__(self, type, value, traceback):
        if time.time() < self.lockexp:
            try:
                self.client.delete(self.key)
            except ConnectionError:
                raise ReleaseLockError('Lock on %s' % self.key)


class LockTimeout(Exception):
    pass


class AcquireLockError(Exception):
    pass


class ReleaseLockError(Exception):
    pass


def create(key, duration=2, timeout=2):
    client = registry.get_registry()['REDIS_LOCK_CLIENT']
    return Lock(client, key, duration, timeout)

# -*- coding: utf-8 -*-
import arrow
import random
from apns import APNs, Payload
from config import Bulbasaur
from lib.models.entry import EntryStatus
from lib.registry import get_registry


class PushNotifications(object):

    def __init__(self):
        self.cert = Bulbasaur.Config.CERT_FILE
        self.key = Bulbasaur.Config.KEY_FILE
        self.use_sandbox = Bulbasaur.Config.APN_USE_SANDBOX
        self.device_repo = get_registry()['DEVICE_REPO']

    def send_notification(self, device_token, message):
        apns = APNs(use_sandbox=self.use_sandbox,
                    cert_file=self.cert,
                    key_file=self.key)
        payload = Payload(alert=message, sound="default")
        apns.gateway_server.send_notification(device_token, payload)

    def send_notifications(self, frame):
        apns = APNs(use_sandbox=self.use_sandbox,
                    cert_file=self.cert,
                    key_file=self.key,
                    enhanced=True)
        apns.gateway_server.send_notification_multiple(frame)

    def add_notification(self, frame, user, count, status, player):
        if not count:
            return frame
        if not player:
            return frame

        if status == EntryStatus.NO_APPEARANCE:
            message = u"%(player)s選手は本日出場機会がありませんでした。 連続記録は%(count)sのままです"  # NOQA
        elif status == EntryStatus.SUCCESS:
            message = u"おめでとうございます！ %(player)s選手が本日ヒットを打ちました! 新しい記録は%(count)sです"  # NOQA
        elif status == EntryStatus.INSURED:
            message = u"%(player)s選手は本日ノーヒットでしたが保険が適応されました。 連続記録は%(count)sのままです"  # NOQA
        elif status == EntryStatus.FAILED:
            message = u"%(player)s選手は本日ノーヒットでしたので記録が途切れてしまいました..."  # NOQA
        else:
            return frame
        message = message % {"player": player.name, "count": count}

        device = self.device_repo.get_latest_device(user)
        if not device:
            return frame
        device_token = device.device_token
        now = arrow.utcnow().timestamp
        expiry = now + 3600
        identifier = random.getrandbits(32)

        payload = Payload(alert=message, sound="default")
        frame.add_item(
            device_token, payload, identifier, expiry, 10)
        return frame

    def add_reminder(self, frame, device_token, num_games):
        message = u"本日は%s試合が行われます。Good Luck!" % num_games
        now = arrow.utcnow().timestamp
        expiry = now + 3600
        identifier = random.getrandbits(32)
        payload = Payload(alert=message, sound="default")
        frame.add_item(
            device_token, payload, identifier, expiry, 10)
        return frame

# -*- coding: utf-8 -*-
import json
import requests
from config import Bulbasaur
from lib.registry import get_registry


class AppleVerify(object):

    def __init__(self):
        self.transaction_repo = get_registry()['TRANSACTION_REPO']

    def verify_receipt(self, user, receipt):
        json_data = json.dumps({'receipt-data': receipt})
        response = requests.post(Bulbasaur.Config.ITUNES_URL, data=json_data)
        if response.status_code != 200:
            # TODO should fire a counter here
            return

        response_data = response.json()

        if response_data.get('status') != 0:
            # TODO should fire a counter here
            return

        receipt_data = response_data.get('receipt')
        if not receipt_data:
            # TODO should fire a counter here
            return

        if receipt_data.get('bundle_id') != Bulbasaur.Config.BUNDLE_ID:
            # TODO should fire a counter here
            return

        purchases = receipt_data.get('in_app')
        if not purchases:
            # TODO should fire a counter here
            return

        if len(purchases) > 1:
            # TODO log here
            pass

        for purchase in purchases:
            product_id = purchase['product_id']
            transaction_id = purchase['transaction_id']
            quantity = int(purchase['quantity'])
            if quantity != 1:
                # TODO log here
                continue
            timestamp_int = int(purchase['purchase_date_ms']) / 1000
            self.transaction_repo.credit_purchase(user, transaction_id,
                                                  product_id, timestamp_int)

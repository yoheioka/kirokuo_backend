# -*- coding: utf-8 -*-
from apns import Frame
from lib.calculator import calculate_insurance_price
from lib.registry import get_registry
from lib.models.entry import EntryStatus
from lib.models.streak import Streak
from lib import redis_lock
from lib.push_notifications import PushNotifications


class EntryFinalizer(object):

    def __init__(self, date):
        self.date = date
        self.db = get_registry()['DB']
        self.game_repo = get_registry()['GAME_REPO']
        self.entry_repo = get_registry()['ENTRY_REPO']
        self.transaction_repo = get_registry()['TRANSACTION_REPO']
        self.player_repo = get_registry()['PLAYER_REPO']
        self.user_repo = get_registry()['USER_REPO']
        self.streak_repo = get_registry()['STREAK_REPO']
        self.push_notifications = PushNotifications()

    def _get_current_streak(self, user, game):
        streak = self.streak_repo.get_latest_streak(user)
        if not streak or streak.ended:
            return Streak(
                email=user.email,
                started_at=game.date,
                updated_at=game.date,
                count=0,
                ended=False,
                insurance_count=0
            )
        return streak

    def _finalize_game(self, game):
        frame = Frame()
        teams = [game.home, game.away]
        players = self.player_repo.get_batters_from_teams(teams)
        players_by_id = {player.id: player for player in players}
        entries = self.entry_repo.get_pending_entries(game)
        had_hit = []
        no_hit = []
        if not game.cancelled:
            player_stats = self.player_repo.get_all_one_day_stats(game)
            had_hit, no_hit = self.player_with_hits_and_no_hits(player_stats)
        for entry in entries:
            key = entry.email
            try:
                with redis_lock.create(key):
                    status = EntryStatus.NO_APPEARANCE
                    if entry.player_id in had_hit:
                        status = EntryStatus.SUCCESS
                    elif entry.player_id in no_hit:
                        status = EntryStatus.FAILED
                        if entry.use_insurance:
                            status = EntryStatus.INSURED

                    user = self.user_repo.get_user(entry.email)
                    streak = self._get_current_streak(user, game)

                    insurance_price = calculate_insurance_price(streak.count)
                    if entry.use_insurance:
                        streak.insurance_count += 1
                        user = self.transaction_repo.use_insurance(
                            user, game, insurance_price)

                    if status == EntryStatus.FAILED:
                        streak.ended = True
                        streak.updated_at = game.date
                    if status == EntryStatus.SUCCESS:
                        streak.count += 1
                        streak.updated_at = game.date

                        user = self.transaction_repo.credit_user_hit_bonus(
                            user)

                        if streak.count > 0 and streak.count % 5 == 0:
                            trans_repo = self.transaction_repo
                            user = trans_repo.credit_user_multi_hit_bonus(
                                user, streak.count)

                    chosen_player = players_by_id.get(entry.player_id)
                    frame = self.push_notifications.add_notification(
                        frame, user, streak.count, status, chosen_player)

                    # do something with transaction and user current balance
                    entry.result = status
                    entry.finalized = True

                    if streak.count > 0:
                        self.db.session.merge(streak)

                    self.db.session.merge(entry)
                    self.db.session.commit()

                    print 'finalized for %s' % user.email
            except redis_lock.LockTimeout:
                print 'Aborting %s. Already running' % key
        if frame.notification_data:
            self.push_notifications.send_notifications(frame)

    def player_with_hits_and_no_hits(self, player_stats):
        had_hit = []
        no_hit = []
        for player_stat in player_stats:
            # no at bats and no sacrifice flies
            if not player_stat.count_appearance:
                continue

            if player_stat.hits > 0:
                had_hit.append(player_stat.id)
            else:
                no_hit.append(player_stat.id)
        return had_hit, no_hit

    def run(self):
        finished_or_cancelled = self.game_repo.get_finished_games(self.date)
        for game in finished_or_cancelled:
            self._finalize_game(game)

# -*- coding: utf-8 -*-
import arrow
from apns import Frame
from lib.registry import get_registry
from lib.models.game import GameStatuses
from lib.push_notifications import PushNotifications


class ReminderHelper(object):

    def __init__(self, date):
        self.date = date
        self.game_repo = get_registry()['GAME_REPO']
        self.device_repo = get_registry()['DEVICE_REPO']
        self.entry_repo = get_registry()['ENTRY_REPO']
        self.push_notifications = PushNotifications()

    def send_reminders(self, email_devices, num_games):
        frame = Frame()
        for device in email_devices.values():
            frame = self.push_notifications.add_reminder(
                frame, device.device_token, num_games)
        if frame.notification_data:
            self.push_notifications.send_notifications(frame)

    def run(self):
        games = self.game_repo.get_games_for_day(self.date)
        if not games:
            return
        start_times = [game.start_time for game in games]
        now = arrow.utcnow()
        if now.timestamp > min(start_times):
            return
        not_started_games = filter(
            lambda game: game.status == GameStatuses.NOT_STARTED, games
        )
        if not not_started_games:
            return
        device_by_email = {}
        for device in self.device_repo.get_all_devices():
            updated_at = device.updated_at
            email = device.email
            if email not in device_by_email:
                device_by_email[email] = device
            else:
                if updated_at < device_by_email[email].updated_at:
                    continue
                device_by_email[email] = device
        entries = self.entry_repo.get_all_entries(self.date)
        already_picked = [entry.email for entry in entries]
        filtered_device_by_email = {
            email: device for email, device in device_by_email.iteritems()
            if email not in already_picked
        }
        self.send_reminders(filtered_device_by_email, len(not_started_games))

# -*- coding: utf-8 -*-
from lib.registry import get_registry
from lib.push_notifications import PushNotifications


class MetricHelper(object):

    def __init__(self, date):
        self.date = date
        self.user_repo = get_registry()['USER_REPO']
        self.entry_repo = get_registry()['ENTRY_REPO']
        self.device_repo = get_registry()['DEVICE_REPO']
        self.push_notifications = PushNotifications()

    def run(self):
        users = self.user_repo.get_users_confirmed_on_date(self.date)
        num_users = len(users)
        entries = self.entry_repo.get_all_entries(self.date)
        num_entries = len(entries)
        message = u"new users: %s\nentries: %s" % (num_users, num_entries)
        device = self.device_repo.get_latest_device_from_email(
            'yoyo716jp@gmail.com'
        )
        if device:
            self.push_notifications.send_notification(
                device.device_token, message)

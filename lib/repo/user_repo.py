# -*- coding: utf-8 -*-
from lib.registry import get_registry
from lib.models.user import User
from config import Bulbasaur


class UserRepo(object):

    def __init__(self):
        self.db = get_registry()['DB']

    def get_user(self, email):
        return User.query.filter(User.email == email).first()

    def add_nickname(self, user, nickname):
        user.nickname = nickname
        self.db.session.merge(user)
        self.db.session.commit()

    def accept_terms(self, user):
        user.accepted_terms = Bulbasaur.Config.MIN_TERMS_REV
        self.db.session.merge(user)
        self.db.session.commit()

    def get_users_confirmed_on_date(self, date):
        return User.query.filter(
            User.confirmed_at > date.timestamp,
            User.confirmed_at < date.replace(days=+1).timestamp
        ).all()

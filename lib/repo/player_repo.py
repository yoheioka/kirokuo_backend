# -*- coding: utf-8 -*-
from lib.registry import get_registry
from lib.models.player import Player
from lib.models.batter_by_day import BatterByDay
from sqlalchemy import Integer, cast, func


class PlayerRepo(object):

    def __init__(self):
        self.db = get_registry()['DB']

    def get_batters_from_teams(self, teams):
        return Player.query.filter(
            Player.team.in_(teams)
        ).order_by(cast(Player.num, Integer)).all()

    def get_batting_stats_for_id(self, batter_id, year):
        bbd = BatterByDay
        return self.db.session.query(
            bbd.id,
            func.sum(bbd.ab),
            func.sum(bbd.h1b + bbd.h2b + bbd.h3b + bbd.hr)
        ).filter(
            bbd.id == batter_id, bbd.year == year
        ).group_by(bbd.id).first()

    def get_batting_stats_for_ids(self, batter_ids, year):
        bbd = BatterByDay
        return self.db.session.query(
            bbd.id,
            func.sum(bbd.ab),
            func.sum(bbd.h1b + bbd.h2b + bbd.h3b + bbd.hr)
        ).filter(
            bbd.id.in_(batter_ids), bbd.year == year
        ).group_by(bbd.id).all()

    def get_player_by_id(self, player_id):
        return Player.query.filter(Player.id == player_id).first()

    def get_all_one_day_stats(self, game):
        bbd = BatterByDay
        return bbd.query.filter(
            bbd.year == game.year,
            bbd.date == game.date,
            bbd.team.in_([game.home, game.away])
        ).all()

    def get_batter_one_day_stat(self, player_id, date):
        bbd = BatterByDay
        return bbd.query.filter(
            bbd.year == date.year,
            bbd.date == date.timestamp,
            bbd.id == player_id
        ).first()

    def remove_stats_for_cancelled_game(self, date, home, away):
        BatterByDay.query.filter(
            BatterByDay.year == date.year,
            BatterByDay.date == date.timestamp,
            BatterByDay.team == home
        ).delete()
        BatterByDay.query.filter(
            BatterByDay.year == date.year,
            BatterByDay.date == date.timestamp,
            BatterByDay.team == away
        ).delete()
        self.db.session.commit()

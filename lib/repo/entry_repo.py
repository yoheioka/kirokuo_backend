# -*- coding: utf-8 -*-
import arrow
from lib.registry import get_registry
from lib.models.entry import Entry, EntryStatus


class EntryRepo(object):

    def __init__(self):
        self.db = get_registry()['DB']

    def update_entry(self, user, game, player, use_insurance):
        entry = Entry(
            year=game.year,
            date=game.date,
            email=user.email,
            game_number=game.game_number,
            game_start=game.start_time,
            player_id=player.id,
            use_insurance=use_insurance,
            entry_at=arrow.utcnow().timestamp,
            result=EntryStatus.PENDING,
            finalized=False
        )

        self.db.session.merge(entry)
        self.db.session.commit()

    def get_entry(self, user, date):
        return Entry.query.filter(
            Entry.year == date.year,
            Entry.date == date.timestamp,
            Entry.email == user.email
        ).first()

    def cancel(self, entry):
        Entry.query.filter(
            Entry.year == entry.year,
            Entry.date == entry.date,
            Entry.email == entry.email
        ).delete()
        self.db.session.commit()

    def get_pending_entries(self, game):
        return Entry.query.filter(
            Entry.year == game.year,
            Entry.date == game.date,
            Entry.game_number == game.game_number,
            Entry.result == EntryStatus.PENDING,
            Entry.finalized == False
        ).all()

    def get_entry_history(self, user):
        return Entry.query.filter(
            Entry.email == user.email
        ).order_by(Entry.date).all()

    def get_all_entries(self, date):
        return Entry.query.filter(
            Entry.year == date.year,
            Entry.date == date.timestamp
        ).all()

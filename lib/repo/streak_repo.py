# -*- coding: utf-8 -*-
import arrow
from lib.registry import get_registry
from lib.models.streak import Streak
from lib.models.user import User
from sqlalchemy import desc, func
from sqlalchemy.orm import join


class StreakRepo(object):

    def __init__(self):
        self.db = get_registry()['DB']

    def get_latest_streak(self, user):
        return Streak.query.filter(
            Streak.email == user.email
        ).order_by(desc(Streak.updated_at)).first()

    def get_active_longest_streaks(self):
        now = arrow.utcnow()
        start = now.floor('year').timestamp
        end = now.ceil('year').timestamp

        return self.db.session.query(
            User.nickname,
            Streak.count
        ).select_from(
            join(Streak, User)
        ).filter(
            Streak.started_at > start,
            Streak.started_at < end,
            Streak.ended == False
        ).order_by(
            desc(Streak.count), User.nickname
        ).limit(30).all()

    def get_all_time_longest_streaks(self):
        now = arrow.utcnow()
        start = now.floor('year').timestamp
        end = now.ceil('year').timestamp

        return self.db.session.query(
            User.nickname,
            func.max(Streak.count)
        ).select_from(
            join(Streak, User)
        ).filter(
            Streak.started_at > start,
            Streak.started_at < end
        ).group_by(
            User.nickname, Streak.email
        ).order_by(
            desc(func.max(Streak.count)), User.nickname
        ).limit(30).all()

# -*- coding: utf-8 -*-
import arrow
import time
import uuid
from lib.models.transaction import Transaction, TransactionType
from lib.registry import get_registry
from sqlalchemy.exc import IntegrityError
from sqlalchemy import desc


class TransactionRepo(object):

    def __init__(self):
        self.db = get_registry()['DB']
        self.transaction_type = TransactionType()

    def credit_user_join_bonus(self, user):
        key = str('%s:%s:join' % (user.email, int(time.time() * 100000)))
        transaction_id = str(uuid.uuid5(uuid.NAMESPACE_DNS, key))
        user = self.credit_user(transaction_id, user, 10,
                                self.transaction_type.JOIN_BONUS,
                                arrow.utcnow().timestamp)
        return user

    def credit_user_hit_bonus(self, user):
        key = str('%s:%s:hit' % (user.email, int(time.time() * 100000)))
        transaction_id = str(uuid.uuid5(uuid.NAMESPACE_DNS, key))
        user = self.credit_user(transaction_id, user, 1,
                                self.transaction_type.HIT_BONUS,
                                arrow.utcnow().timestamp)
        return user

    def credit_user_multi_hit_bonus(self, user, streak):
        key = str('%s:%s:hit' % (user.email, int(time.time() * 100000)))
        transaction_id = str(uuid.uuid5(uuid.NAMESPACE_DNS, key))
        transaction_type = self.transaction_type.generate_multi_hit_reason(
            streak)
        user = self.credit_user(transaction_id, user, streak,
                                transaction_type,
                                arrow.utcnow().timestamp)
        return user

    def use_insurance(self, user, game, insurance_price):
        key = str('%s:%s:insurace' % (user.email, int(time.time() * 100000)))
        transaction_id = str(uuid.uuid5(uuid.NAMESPACE_DNS, key))
        user = self.credit_user(transaction_id, user, -insurance_price,
                                self.transaction_type.INSURANCE, game.date,
                                commit=False)
        return user

    def credit_purchase(self, user, transaction_id, product_id, timestamp_int):
        amount = self.transaction_type.TRANSACTION_ID_TO_AMOUNT[product_id]
        user = self.credit_user(transaction_id, user, amount,
                                product_id, timestamp_int)
        return user

    def credit_user(self, transaction_id, user, amount, reason, timestamp_int,
                    commit=True):
        transaction = Transaction(
            id=transaction_id,
            user_email=user.email,
            date=timestamp_int,
            amount=amount,
            reason=reason,
            updated_at=arrow.utcnow().timestamp
        )
        user.current_balance = user.current_balance + amount

        try:
            self.db.session.add(transaction)
            self.db.session.merge(user)
            if commit:
                self.db.session.commit()
        except IntegrityError:
            # TODO should log here
            pass
        return user

    def get_all_transactions(self, user):
        return Transaction.query.filter(
            Transaction.user_email == user.email
        ).order_by(
            desc(Transaction.updated_at),
            desc(Transaction.amount)
        ).all()

# -*- coding: utf-8 -*-
import arrow
from lib.registry import get_registry
from lib.models.device import Device
from sqlalchemy import desc


class DeviceRepo(object):

    def __init__(self):
        self.db = get_registry()['DB']

    def register_device(self, user, device_token):
        device = Device(email=user.email,
                        device_token=device_token,
                        updated_at=arrow.utcnow().timestamp)
        self.db.session.merge(device)
        self.db.session.commit()

    def get_latest_device(self, user):
        return Device.query.filter(
            Device.email == user.email
        ).order_by(desc(Device.updated_at)).first()

    def get_latest_device_from_email(self, email):
        return Device.query.filter(
            Device.email == email
        ).order_by(desc(Device.updated_at)).first()

    def get_all_devices(self):
        return Device.query

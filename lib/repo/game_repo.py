# -*- coding: utf-8 -*-
from lib.registry import get_registry
from lib.models.game import Game, GameStatuses


class GameRepo(object):

    def __init__(self):
        self.db = get_registry()['DB']

    def get_games_for_day(self, date):
        games = Game.query.filter(
            Game.year == date.year, Game.date == date.timestamp
        ).order_by(Game.game_number).all()
        return games

    def get_game(self, date, game_number):
        return Game.query.filter(
            Game.year == date.year,
            Game.date == date.timestamp,
            Game.game_number == game_number
        ).first()

    def get_game_for_teams(self, date, home, away):
        return Game.query.filter(
            Game.year == date.year,
            Game.date == date.timestamp,
            Game.home == home,
            Game.away == away
        ).first()

    def get_finished_games(self, date):
        games = Game.query.filter(
            Game.year == date.year,
            Game.date == date.timestamp,
            Game.status.in_([GameStatuses.COMPLETED, GameStatuses.CANCELLED])
        ).order_by(Game.game_number).all()
        return games

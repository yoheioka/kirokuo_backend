# -*- coding: utf-8 -*-
from config.application import GlobalApplicationConfig, GlobalLogConfig
import logging


class ApplicationConfig(GlobalApplicationConfig):
    ITUNES_URL = 'https://sandbox.itunes.apple.com/verifyReceipt'
    SQLALCHEMY_DATABASE_URI = 'postgresql://localhost/bulbasaur'
    DEBUG = True

    # MAIL
    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USE_SSL = True
    MAIL_USERNAME = 'TODO'
    MAIL_PASSWORD = 'TODO'

    CERT_FILE = 'pems/cert_dev.pem'
    KEY_FILE = 'pems/pkey_dev.pem'
    APN_USE_SANDBOX = True


class LogConfig(GlobalLogConfig):
    LOG_TO_FILE = True
    LOG_TO_CONSOLE = True
    ROOT_LEVEL = logging.INFO

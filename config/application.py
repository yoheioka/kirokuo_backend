# -*- coding: utf-8 -*-
import logging


class GlobalApplicationConfig(object):
    """ Application Defaults """

    SQLALCHEMY_POOL_RECYCLE = 3600
    WTF_CSRF_ENABLED = False
    SECRET_KEY = 'secret'
    SECURITY_PASSWORD_HASH = 'pass_hash'
    SECURITY_PASSWORD_SALT = 'something_super_secret_change_in_production'
    SECURITY_CONFIRMABLE = True
    FACEBOOK_KEY = 'fb_key'
    PASSWORD_PLACEHOLDER = 'placeholder'

    # Redis Configs
    REDIS_HOST = '127.0.0.1'
    REDIS_PORT = 6379
    REDIS_DB = 0

    REDIS_LOCK_HOST = '127.0.0.1'
    REDIS_LOCK_PORT = 6379
    REDIS_LOCK_DB = 1

    BUNDLE_ID = 'com.mayoapps.kirokuo'
    MIN_VERSION = 1.01

    AWS_REGION = 'ap-northeast-1'
    AWS_ACCESS_KEY_ID = 'access_key'
    AWS_SECRET_ACCESS_KEY = 'secret_key'

    MIN_TERMS_REV = 1


class GlobalLogConfig(object):

    FORMAT = '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
    ROOT_LEVEL = logging.DEBUG
    LOG_TO_CONSOLE = True
    CONSOLE_LEVEL = logging.DEBUG
    LOG_TO_FILE = False
    FILE_BASE = '/var/log/bulbasaur/'
    FILE_LEVEL = logging.DEBUG

# -*- coding: utf-8 -*-
from lib import Log


class Bulbasaur(object):
    Config = None
    LogConfig = None

    @staticmethod
    def init_config(env):
        if env == 'development':
            import development as env_config
        elif env == 'production':
            import production as env_config
        else:
            raise Exception("Environment not found: %s" % env)

        Bulbasaur.Config = env_config.ApplicationConfig
        Bulbasaur.LogConfig = env_config.LogConfig


def init_config(env=None):
    init_constants(env)
    Log.init(Bulbasaur.LogConfig)


def init_constants(env):
    Bulbasaur.init_config(env)

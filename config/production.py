# -*- coding: utf-8 -*-
from config.application import GlobalApplicationConfig, GlobalLogConfig
import logging


class ApplicationConfig(GlobalApplicationConfig):
    ITUNES_URL = 'https://buy.itunes.apple.com/verifyReceipt'

    DEBUG = False

    # Redis Configs
    REDIS_HOST = 'TODO'
    REDIS_PORT = 6379
    REDIS_DB = 0

    REDIS_LOCK_HOST = 'TODO'
    REDIS_LOCK_PORT = 6379
    REDIS_LOCK_DB = 1

    # MAIL
    SMTP_SERVER = 'smtp.mailgun.com'
    SMTP_LOGIN = 'TODO'
    SMTP_PASSWORD = 'TODO'

    MAIL_SERVER = 'smtp.mailgun.com'
    MAIL_USERNAME = 'TODO'
    MAIL_PASSWORD = 'TODO'

    MAIL_PORT = 587
    MAIL_USE_TLS = True

    CERT_FILE = 'pems/cert_prod.pem'
    KEY_FILE = 'pems/pkey_prod.pem'
    APN_USE_SANDBOX = False


class LogConfig(GlobalLogConfig):
    LOG_TO_FILE = True
    LOG_TO_CONSOLE = False
    ROOT_LEVEL = logging.INFO
    FILE_LEVEL = logging.INFO

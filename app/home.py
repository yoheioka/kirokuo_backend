# -*- coding: utf-8 -*-
import base64
import pickle
from flask import Blueprint, request, Response, render_template
import logging
from lib.email import EmailHelper
_logger = logging.getLogger(__name__)
home = Blueprint('home', __name__, template_folder='templates')


@home.route('/')
def index():
    return render_template('home.html')


@home.route('/coming_soon')
def coming_soon():
    return render_template('coming_soon.html')


@home.route('/present')
def present():
    return render_template('present.html')


@home.route('/rule')
def rule():
    return render_template('rule.html')


@home.route('/privacy_policy')
def privacy_policy():
    return render_template('privacy_policy.html')


@home.route('/terms_of_use')
def terms_of_use():
    return render_template('terms_of_use.html')


@home.route('/send_confirmation_email', methods=['POST'])
def send_confirmation_email():
    pickled_data = base64.b64decode(request.data)
    data = pickle.loads(pickled_data)

    try:
        pickled_data = base64.b64decode(request.data)
        data = pickle.loads(pickled_data)
        sender = data['sender']
        email = data['email']
        confirmation_code = data['confirmation_code']
        topic = data['topic']

        _logger.info("START sending to: %s", email)

        email_helper = EmailHelper()
        if topic == 'confirmation':
            email_helper.send_confirmation_email(
                sender, email, confirmation_code)
        elif topic == 'resend_confirmation':
            email_helper.resend_confirmation_email(
                sender, email, confirmation_code)
        elif topic == 'password_confirmation':
            email_helper.send_password_confirmation_email(
                sender, email, confirmation_code)

        _logger.info("FINISHED sending to: %s", email)
        response = Response("", status=200)
    except Exception as e:
        _logger.exception(e)
        # logging.exception('Error processing message: %s' % request.json)
        response = Response(e.message, status=500)

    return response

# -*- coding: utf-8 -*-
from lib.apple import AppleVerify
from lib.registry import get_registry


class TransactionManager(object):

    def __init__(self):
        self.transaction_repo = get_registry()['TRANSACTION_REPO']
        self.apple_verify = AppleVerify()

    def get_all_transactions(self, user):
        return self.transaction_repo.get_all_transactions(user)

    def verify_in_app_purchase(self, user, receipt):
        self.apple_verify.verify_receipt(user, receipt)

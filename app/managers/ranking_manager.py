# -*- coding: utf-8 -*-
from lib.registry import get_registry


class RankingManager(object):

    def __init__(self):
        registry = get_registry()
        self.streak_repo = registry['STREAK_REPO']

    def get_ranking(self):
        active_ranking = self.streak_repo.get_active_longest_streaks()
        all_time_ranking = self.streak_repo.get_all_time_longest_streaks()

        return (
            self._process_ranking_tuple(active_ranking),
            self._process_ranking_tuple(all_time_ranking)
        )

    def _process_ranking_tuple(self, ranking_tuples):
        ranking = []
        count = 0
        current_streak = None
        for nickname, streak in ranking_tuples:
            count += 1
            if not current_streak:
                current_streak = streak
                current_rank = 1
            if streak < current_streak:
                current_rank = count
                current_streak = streak
            ranking.append({
                'rank': current_rank,
                'nickname': nickname,
                'streak': streak
            })
        return ranking

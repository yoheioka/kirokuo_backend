# -*- coding: utf-8 -*-
import arrow
import random
import string
from config import Bulbasaur
import lib.exceptions as exceptions
from lib.email import EmailHelper
from lib.registry import get_registry
from lib.facebook import get_facebook_verification_data
from lib.google import get_google_verification_data
from lib.time_utils import get_date_in_japan
from flask.ext.security.utils import encrypt_password, login_user
from flask.ext.security.forms import LoginForm
from sqlalchemy.exc import IntegrityError
from werkzeug.datastructures import MultiDict


class AccountManager(object):

    FACEBOOK = 'facebook'
    GOOGLE = 'google'

    def __init__(self):
        registry = get_registry()
        self.user_datastore = registry['USER_DATASTORE']
        self.db = registry['DB']
        self.email_helper = EmailHelper()
        self.transaction_repo = registry['TRANSACTION_REPO']
        self.entry_repo = registry['ENTRY_REPO']
        self.player_repo = registry['PLAYER_REPO']
        self.streak_repo = registry['STREAK_REPO']
        self.user_repo = registry['USER_REPO']
        self.device_repo = registry['DEVICE_REPO']

    def signup(self, email, password):
        if not email or not password:
            raise exceptions.MissingEmailOrPassword()

        user = self._create_new_account(email, password=password)
        self.email_helper.queue_email(
            email, user.confirmation_code, 'confirmation')

    def signin(self, signin_params):
        form = LoginForm(MultiDict(signin_params))

        if form.validate_on_submit():
            form_user = form.user
            if self._is_facebook_account(form_user):
                raise exceptions.FacebookAccountExists()
            if self._is_google_account(form_user):
                raise exceptions.GoogleAccountExists()

            login_user(form_user, remember=form.remember.data)
            self.db.session.commit()
            return form_user.get_auth_token()

        if form.errors:
            email_errors = form.errors.get('email') or []
            if 'Email requires confirmation.' in email_errors:
                raise exceptions.NotConfirmed()
            if 'Specified user does not exist' in email_errors:
                raise exceptions.UserDoesntExist()
        raise exceptions.LoginFailed()

    def check_confirmation_code(self, email, conf_code):
        user = self.user_datastore.get_user(email)
        if not user:
            raise exceptions.InvalidConfirmationCode()
        user.confirmation_attempt += 1
        self.db.session.add(user)
        self.db.session.commit()
        if user.confirmation_attempt > 10:
            raise exceptions.TooManyConfirmationAttempts()
        if user.confirmation_code != conf_code:
            raise exceptions.InvalidConfirmationCode()
        if not user.confirmed_at:
            user.confirmed_at = arrow.utcnow().timestamp
            self.db.session.commit()
        return user.get_auth_token()

    def check_password_confirmation_code(self, email, conf_code):
        user = self.user_datastore.get_user(email)
        if not user:
            raise exceptions.InvalidConfirmationCode()
        user.password_confirmation_attempt += 1
        self.db.session.add(user)
        self.db.session.commit()
        if user.password_confirmation_attempt > 10:
            raise exceptions.TooManyConfirmationAttempts()
        if user.password_confirmation_code != conf_code:
            raise exceptions.InvalidConfirmationCode()
        user.password_confirmation_attempt = 0
        if not user.confirmed_at:
            user.confirmed_at = arrow.utcnow().timestamp
        self.db.session.add(user)
        self.db.session.commit()
        return user.get_auth_token()

    def send_new_confirmation_email(self, email):
        if not email:
            raise exceptions.MissingEmailOrPassword()
        user = self.user_datastore.get_user(email)
        if not user:
            raise exceptions.UserDoesntExist()
        if user.confirmed_at:
            if self._is_facebook_account(user):
                raise exceptions.FacebookAccountExists()
            if self._is_google_account(user):
                raise exceptions.GoogleAccountExists()
            raise exceptions.AlreadyConfirmed()

        self.email_helper.queue_email(
            email, user.confirmation_code, 'resend_confirmation')

    def send_password_confirmation_email(self, email):
        if not email:
            raise exceptions.MissingEmailOrPassword()
        user = self.user_datastore.get_user(email)
        if not user:
            raise exceptions.UserDoesntExist()
        if self._is_facebook_account(user):
            raise exceptions.FacebookAccountExists()
        if self._is_google_account(user):
            raise exceptions.GoogleAccountExists()

        user.password_confirmation_code = self._generate_confirmation_code()
        self.db.session.add(user)

        self.db.session.commit()
        self.email_helper.queue_email(
            email, user.password_confirmation_code, 'password_confirmation')

    def change_password(self, user, password):
        user.password = encrypt_password(password)
        self.db.session.add(user)
        self.db.session.commit()
        return user.get_auth_token()

    def login_with_facebook(self, access_token):
        if not access_token:
            raise exceptions.MissingAccessToken()

        fb_data = get_facebook_verification_data(access_token)
        if not fb_data:
            raise exceptions.AuthenticationFailed()
        if not fb_data.get('verified'):
            raise exceptions.NotVerified()

        if not fb_data.get('email'):
            raise exceptions.NoEmailAccount()

        email = fb_data['email'].lower()
        user = self.user_datastore.get_user(email)

        if user:
            if self._is_google_account(user):
                raise exceptions.GoogleAccountExists()

            if user and user.source == 'facebook':
                return user.get_auth_token()

            # if normal account already exists with this email address
            raise exceptions.AccountExists()

        user = self._create_new_account(email, source=self.FACEBOOK)
        return user.get_auth_token()

    def _is_facebook_account(self, user):
        return user.source == self.FACEBOOK

    def login_with_google(self, access_token):
        if not access_token:
            raise exceptions.MissingAccessToken()

        google_data = get_google_verification_data(access_token)
        if not google_data:
            raise exceptions.AuthenticationFailed()

        if not google_data.get('verified'):
            raise exceptions.NotVerified()

        email = google_data['email'].lower()
        user = self.user_datastore.get_user(email)

        if user:
            if user.source == 'google':
                return user.get_auth_token()

            if user.source == 'facebook':
                raise exceptions.FacebookAccountExists()

            # if normal account already exists with this email address
            raise exceptions.AccountExists()

        user = self._create_new_account(email, source=self.GOOGLE)
        return user.get_auth_token()

    def _is_google_account(self, user):
        return user.source == self.GOOGLE

    def _generate_confirmation_code(self):
        return ''.join(random.choice(string.digits) for _ in range(6))

    def _strip_email(self, email):
        at_split = email.split('@')
        username = at_split[0].split('+')[0].replace('.', '')
        return '%s@%s' % (username, at_split[-1])

    def _create_new_account(self, email, password='', source=''):
        try:
            if source in (self.FACEBOOK, self.GOOGLE):
                user = self.user_datastore.create_user(
                    email=email,
                    stripped_email=self._strip_email(email),
                    password=encrypt_password(
                        '%s%s%s' % (email,
                                    source,
                                    Bulbasaur.Config.PASSWORD_PLACEHOLDER)),
                    source=source,
                    confirmed_at=arrow.utcnow().timestamp)

            else:
                user = self.user_datastore.create_user(
                    email=email,
                    stripped_email=self._strip_email(email),
                    password=encrypt_password(password),
                    confirmation_code=self._generate_confirmation_code()
                )
            self.db.session.commit()
            self.user_datastore.add_role_to_user(email, 'user')
            self.db.session.commit()
            self.transaction_repo.credit_user_join_bonus(user)
            return user
        except IntegrityError:
            raise exceptions.AccountExists()
        except:
            raise

    def get_entry_dict(self, user):
        date = get_date_in_japan()
        entry = self.entry_repo.get_entry(user, date)
        if not entry:
            return None
        player = self.player_repo.get_player_by_id(entry.player_id)
        player_stats = self.player_repo.get_batting_stats_for_id(player.id,
                                                                 date.year)
        one_day_stat = self.player_repo.get_batter_one_day_stat(player.id,
                                                                date)
        today_hits = 0
        today_abs = 0
        if one_day_stat:
            today_abs = one_day_stat.ab
            today_hits = one_day_stat.hits
        stats = {}
        if player_stats:
            _, ab, hits = player_stats
            stats = {'at_bats': ab, 'hits': hits}
        entry_dict = player.to_dict(stats=stats)
        entry_dict.update({
            'result': entry.result,
            'locked': entry.locked,
            'use_insurance': entry.use_insurance,
            'today_hits': today_hits,
            'today_abs': today_abs,
            'before_match': arrow.utcnow() < arrow.get(entry.game_start)
        })
        return entry_dict

    def get_current_streak_count(self, user):
        streak = self.streak_repo.get_latest_streak(user)
        count = 0
        if streak and not streak.ended:
            count = streak.count
        return count

    def add_nickname(self, user, nickname):
        try:
            self.user_repo.add_nickname(user, nickname)
        except IntegrityError:
            raise exceptions.NicknameExists()

    def accept_terms(self, user):
        self.user_repo.accept_terms(user)

    def register_device(self, user, device_token):
        self.device_repo.register_device(user, device_token)

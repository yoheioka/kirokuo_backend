# -*- coding: utf-8 -*-
import arrow
import lib.exceptions as exceptions
from lib.time_utils import get_date_in_japan
from lib.registry import get_registry
from lib.calculator import calculate_insurance_price


class GameManager(object):

    def __init__(self):
        registry = get_registry()
        self.game_repo = registry['GAME_REPO']
        self.player_repo = registry['PLAYER_REPO']
        self.entry_repo = registry['ENTRY_REPO']
        self.streak_repo = registry['STREAK_REPO']

    def get_today_games(self):
        if arrow.utcnow().hour >= 15 and arrow.utcnow().hour < 17:
            raise exceptions.GameNotReady()
        date = get_date_in_japan()
        return self.game_repo.get_games_for_day(date)

    def get_game_for_game_number(self, game_number):
        if arrow.utcnow().hour >= 15 and arrow.utcnow().hour < 17:
            raise exceptions.GameNotReady()
        date = get_date_in_japan()
        return self.game_repo.get_game(date, game_number)

    def get_player_for_id(self, player_id):
        return self.player_repo.get_player_by_id(player_id)

    def get_batters_for_game(self, game):
        teams = [game.home, game.away]
        players = self.player_repo.get_batters_from_teams(teams)
        return filter(lambda x: x.is_batter, players)

    def get_batting_stats_by_id(self, batters):
        year = get_date_in_japan().year
        batter_ids = set([batter.id for batter in batters])
        stats_tuple_list = self.player_repo.get_batting_stats_for_ids(
            batter_ids, year)
        stats_by_id = {
            b_id: {'at_bats': int(ab), 'hits': int(hits)}
            for (b_id, ab, hits) in stats_tuple_list
        }
        return stats_by_id

    def verify_game_and_player(self, game, player):
        if not game:
            raise exceptions.GameDoesntExist()
        if not player:
            raise exceptions.PlayerDoesntExist()
        if player.team not in (game.away, game.home):
            raise exceptions.PlayerNotInGame()

        if game.locked:
            raise exceptions.GameLocked()

    def save_entry(self, user, game, player, use_insurance):
        streak = self.streak_repo.get_latest_streak(user)
        if use_insurance:
            price = self._calculate_insurance_price(streak)
            if price > user.current_balance:
                raise exceptions.NotEnoughMedals()
            if streak and not streak.ended and streak.insurance_count:
                raise exceptions.InsuranceAlreadyUsed()
        self.entry_repo.update_entry(user, game, player, use_insurance)

    def get_entry_for_today(self, user):
        date = get_date_in_japan()
        return self.entry_repo.get_entry(user, date)

    def cancel_entry(self, entry):
        self.entry_repo.cancel(entry)

    def get_entry_history(self, user):
        entries = self.entry_repo.get_entry_history(user)
        entries_with_streak = []
        streak_count = 0
        player_cache = {}
        for entry in entries:
            if entry.failed:
                streak_count = 0
            if entry.success:
                streak_count += 1
            player_id = entry.player_id
            if player_id not in player_cache:
                player = self.player_repo.get_player_by_id(player_id)
                player_cache[player_id] = player
            else:
                player = player_cache[player_id]
            player_stats = self.player_repo.get_batter_one_day_stat(
                player_id, arrow.get(entry.date))
            entries_with_streak.append(
                (entry, streak_count, player, player_stats)
            )
        entries_with_streak.reverse()
        return entries_with_streak

    def get_insurance_price(self, user):
        streak = self.streak_repo.get_latest_streak(user)
        return self._calculate_insurance_price(streak)

    def _calculate_insurance_price(self, streak):
        streak_count = 0
        if streak and not streak.ended:
            streak_count = streak.count
        return calculate_insurance_price(streak_count)

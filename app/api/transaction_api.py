# -*- coding: utf-8 -*-
from flask import Blueprint, current_app, jsonify, request
from flask_security.core import current_user
from flask_security.decorators import auth_token_required
import logging
_logger = logging.getLogger(__name__)
transaction = Blueprint('transaction', __name__)


@transaction.route('/get_history', methods=['POST'])
@auth_token_required
def get_history():
    transaction_manager = current_app.managers.transaction_manager
    try:
        all_transactions = [
            transaction.to_dict() for transaction in
            transaction_manager.get_all_transactions(current_user)
        ]
        return jsonify({'result': all_transactions})
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@transaction.route('/validate_transaction', methods=['POST'])
@auth_token_required
def validate_transaction():
    transaction_manager = current_app.managers.transaction_manager
    params = request.json
    try:
        receipt = params.get('receipt')
        transaction_manager.verify_in_app_purchase(current_user, receipt)

        return jsonify({'result': True})
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400

# -*- coding: utf-8 -*-
from flask import Blueprint, jsonify
import logging
from lib.rule_info import RULES
from lib.present_info import PRESENTS
from lib.terms_info import TERMS
_logger = logging.getLogger(__name__)
info_api = Blueprint('info', __name__)


@info_api.route('/get_rules', methods=['POST'])
def get_rules():
    return jsonify({'result': RULES})


@info_api.route('/get_presents_info', methods=['POST'])
def get_presents_info():
    return jsonify({'result': PRESENTS})


@info_api.route('/get_terms', methods=['POST'])
def get_terms():
    return jsonify({'result': TERMS})

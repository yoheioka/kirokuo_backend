# -*- coding: utf-8 -*-
import lib.exceptions as exceptions
from flask import Blueprint, current_app, jsonify, request
from flask_security.core import current_user
from flask_security.decorators import auth_token_required
import logging
_logger = logging.getLogger(__name__)
game_api = Blueprint('game', __name__)


@game_api.route('/todays_games', methods=['POST'])
@auth_token_required
def todays_games():
    game_manager = current_app.managers.game_manager
    try:
        all_games = [game.to_dict() for game in game_manager.get_today_games()]
        return jsonify({'result': all_games})
    except (
        exceptions.GameNotReady
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@game_api.route('/get_batters', methods=['POST'])
@auth_token_required
def get_batters():
    params = request.json
    game_number = params.get('game_number')
    game_manager = current_app.managers.game_manager
    try:
        game = game_manager.get_game_for_game_number(game_number)
        if not game:
            raise exceptions.GameDoesntExist()
        batters = game_manager.get_batters_for_game(game)
        batting_stats_by_id = game_manager.get_batting_stats_by_id(batters)
        home_batters = []
        away_batters = []
        for batter in batters:
            stats = batting_stats_by_id.get(batter.id, {})
            if batter.team == game.home:
                home_batters.append(batter.to_dict(stats=stats))
            elif batter.team == game.away:
                away_batters.append(batter.to_dict(stats=stats))

        return jsonify({
            'home': game.home,
            'away': game.away,
            'start_time': game.start_time,
            'home_batters': home_batters,
            'away_batters': away_batters,
            'insurance_price': game_manager.get_insurance_price(current_user)
        })
    except (
        exceptions.GameDoesntExist,
        exceptions.GameNotReady
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@game_api.route('/make_entry', methods=['POST'])
@auth_token_required
def make_entry():
    params = request.json
    game_number = params.get('game_number')
    player_id = params.get('player_id')
    use_insurance = params.get('use_insurance')
    game_manager = current_app.managers.game_manager
    try:
        entry = game_manager.get_entry_for_today(current_user)
        if entry and entry.locked:
            raise exceptions.EntryLocked()

        game = game_manager.get_game_for_game_number(game_number)
        player = game_manager.get_player_for_id(player_id)
        game_manager.verify_game_and_player(game, player)
        game_manager.save_entry(current_user, game, player, use_insurance)
        return jsonify({'result': True})
    except (
        exceptions.GameDoesntExist,
        exceptions.PlayerDoesntExist,
        exceptions.GameLocked,
        exceptions.EntryLocked,
        exceptions.GameNotReady,
        exceptions.NotEnoughMedals,
        exceptions.InsuranceAlreadyUsed
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@game_api.route('/cancel_entry', methods=['POST'])
@auth_token_required
def cancel_entry():
    game_manager = current_app.managers.game_manager
    try:
        entry = game_manager.get_entry_for_today(current_user)
        if not entry:
            raise exceptions.EntryDoesntExist()
        if entry.locked:
            raise exceptions.EntryLocked()

        game_manager.cancel_entry(entry)
        return jsonify({'result': True})
    except (
        exceptions.EntryDoesntExist,
        exceptions.EntryLocked
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@game_api.route('/entry_history', methods=['POST'])
@auth_token_required
def entry_history():
    game_manager = current_app.managers.game_manager
    try:
        entries_with_streak = game_manager.get_entry_history(current_user)
        entries = [
            entry.to_dict(streak_count, player, player_stats)
            for entry, streak_count, player, player_stats
            in entries_with_streak
        ]
        return jsonify({'result': entries})
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400

# -*- coding: utf-8 -*-
from config import Bulbasaur
import lib.exceptions as exceptions
from flask import Blueprint, current_app, request, jsonify
from flask_security.core import current_user
from flask_security.decorators import auth_token_required
import logging
_logger = logging.getLogger(__name__)
account = Blueprint('account', __name__)


@account.route('/')
def index():
    return jsonify({'result': True})


@account.route('/refresh_user', methods=['POST'])
@auth_token_required
def refresh_user():
    """
        entry_dict: {
            'at_bats': 0,
            'batting': u'\u53f3',
            'hits': 0,
            'id': 900051,
            'name': u'\u5c71\u5d0e\u61b2\u6674',
            'num': u'0',
            'position': u'\u5185\u91ce\u624b',
            'result': u'pending',
            'team': u'DB',
            'use_insurance': True
         }
    """
    params = request.json
    account_manager = current_app.managers.account_manager
    try:
        device_token = params.get("device_token")
        if device_token:
            account_manager.register_device(current_user, device_token)

        version = float(params['version'])
        if version < Bulbasaur.Config.MIN_VERSION:
            raise exceptions.UpdateRequired()
        if not current_user.accepted_recent_terms:
            raise exceptions.TermsNotAccepted()
        if not current_user.nickname:
            raise exceptions.MissingNickname()
        entry_dict = account_manager.get_entry_dict(current_user)

        return jsonify({
            'email': current_user.email,
            'current_balance': current_user.current_balance,
            'entry': entry_dict,
            'current_streak': account_manager.get_current_streak_count(
                current_user),
            'nickname': current_user.nickname
        })
    except (
        exceptions.UpdateRequired,
        exceptions.MissingNickname,
        exceptions.TermsNotAccepted
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/accept_terms', methods=['POST'])
@auth_token_required
def accept_terms():
    account_manager = current_app.managers.account_manager
    try:
        account_manager.accept_terms(current_user)
        return jsonify({'result': True})
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/signin', methods=['POST'])
def signin():
    params = request.json

    try:
        params['email'] = params.get('email', '').lower()
        account_manager = current_app.managers.account_manager
        auth_token = account_manager.signin(params)
        return jsonify({'auth_token': auth_token})
    except (
        exceptions.FacebookAccountExists,
        exceptions.GoogleAccountExists,
        exceptions.NotConfirmed,
        exceptions.UserDoesntExist,
        exceptions.LoginFailed
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/signup', methods=['POST'])
def signup():
    params = request.json

    try:
        email = params.get('email', '').lower()
        password = params.get('password')
        account_manager = current_app.managers.account_manager
        account_manager.signup(email, password)
        return jsonify({'result': 'success'})
    except exceptions.AccountExists as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/fb_signin', methods=['POST'])
def fb_signin():
    params = request.json
    access_token = params.get('fb_access_token')
    account_manager = current_app.managers.account_manager

    try:
        auth_token = account_manager.login_with_facebook(access_token)
        return jsonify({'auth_token': auth_token})
    except (
        exceptions.MissingAccessToken,
        exceptions.AuthenticationFailed,
        exceptions.AccountExists,
        exceptions.NotVerified,
        exceptions.NoEmailAccount,
        exceptions.GoogleAccountExists
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/google_signin', methods=['POST'])
def google_signin():
    params = request.json
    access_token = params.get('google_access_token')
    account_manager = current_app.managers.account_manager

    try:
        auth_token = account_manager.login_with_google(access_token)
        return jsonify({'auth_token': auth_token})
    except (
        exceptions.MissingAccessToken,
        exceptions.AuthenticationFailed,
        exceptions.AccountExists,
        exceptions.NotVerified,
        exceptions.FacebookAccountExists
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/resend_confirmation_email', methods=['POST'])
def resend_confirmation_email():
    params = request.json
    email = params.get('email')
    account_manager = current_app.managers.account_manager

    try:
        account_manager.send_new_confirmation_email(email)
        return jsonify({'result': 'success'})
    except (
        exceptions.MissingEmailOrPassword,
        exceptions.UserDoesntExist,
        exceptions.FacebookAccountExists,
        exceptions.GoogleAccountExists,
        exceptions.AlreadyConfirmed
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/send_password_confirmation', methods=['POST'])
def send_password_confirmation():
    params = request.json
    email = params.get('email')
    account_manager = current_app.managers.account_manager

    try:
        account_manager.send_password_confirmation_email(email)
        return jsonify({'result': 'success'})
    except (
        exceptions.MissingEmailOrPassword,
        exceptions.UserDoesntExist,
        exceptions.FacebookAccountExists,
        exceptions.GoogleAccountExists
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/validate_confirmation_code', methods=['POST'])
def validate_confirmation_code():
    params = request.json
    email = params.get('email')
    conf_code = params.get('confirmation_code')
    account_manager = current_app.managers.account_manager

    try:
        auth_token = account_manager.check_confirmation_code(email, conf_code)
        return jsonify({'auth_token': auth_token})
    except (
        exceptions.InvalidConfirmationCode,
        exceptions.TooManyConfirmationAttempts
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/validate_password_confirmation_code', methods=['POST'])
def validate_password_confirmation_code():
    params = request.json
    email = params.get('email')
    conf_code = params.get('confirmation_code')
    account_manager = current_app.managers.account_manager

    try:
        auth_token = account_manager.check_password_confirmation_code(
            email, conf_code)
        return jsonify({'auth_token': auth_token})
    except (
        exceptions.InvalidConfirmationCode,
        exceptions.TooManyConfirmationAttempts
    ) as e:
        _logger.info(e)
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/change_password', methods=['POST'])
@auth_token_required
def change_password():
    params = request.json
    password = params.get('password')
    account_manager = current_app.managers.account_manager

    try:
        auth_token = account_manager.change_password(current_user, password)
        return jsonify({'auth_token': auth_token})
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400


@account.route('/add_nickname', methods=['POST'])
@auth_token_required
def add_nickname():
    params = request.json
    nickname = params.get('nickname')
    account_manager = current_app.managers.account_manager

    try:
        if not nickname:
            raise exceptions.MissingNickname()
        account_manager.add_nickname(current_user, nickname)
        return jsonify({'result': True})
    except (
        exceptions.MissingNickname,
        exceptions.NicknameExists
    ) as e:
        _logger.info(e)
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400

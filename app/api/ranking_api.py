# -*- coding: utf-8 -*-
import lib.exceptions as exceptions
from flask import Blueprint, current_app, jsonify
import logging
_logger = logging.getLogger(__name__)
ranking_api = Blueprint('ranking', __name__)


@ranking_api.route('/get_ranking', methods=['POST'])
def get_ranking():
    ranking_manager = current_app.managers.ranking_manager
    try:
        active_ranking, all_time_ranking = ranking_manager.get_ranking()
        return jsonify({
            'active': active_ranking,
            'all_time': all_time_ranking
        })
    except (
        exceptions.GameNotReady
    ) as e:
        return jsonify({'error': e.message}), 400
    except Exception as e:
        # TODO
        # report to sentry
        _logger.exception(e)
        return jsonify({'error': 'unknown_error'}), 400

# -*- coding: utf-8 -*-
import redis
from lib.registry import get_registry
from config import Bulbasaur
from config.app_config import init_config
from flask import Flask, render_template
from flask.ext.security import SQLAlchemyUserDatastore, Security
from flask.ext.sqlalchemy import SQLAlchemy
from flask_mail import Mail
import logging


def _initialize_flask_app():
    return Flask(__name__)


def _register_version(app):
    from version import version
    app.add_url_rule('/version.txt', 'version', version)
    return app


def _register_blueprints(app):
    from home import home
    app.register_blueprint(home)

    from api.account_api import account
    from api.transaction_api import transaction
    from api.game_api import game_api
    from api.info_api import info_api
    from api.ranking_api import ranking_api
    app.register_blueprint(account, url_prefix='/api/v1/account')
    app.register_blueprint(transaction, url_prefix='/api/v1/transaction')
    app.register_blueprint(game_api, url_prefix='/api/v1/game')
    app.register_blueprint(info_api, url_prefix='/api/v1/info')
    app.register_blueprint(ranking_api, url_prefix='/api/v1/ranking')

    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('404.html'), 404

    return app


def _initialize_managers(app):
    from app.managers.account_manager import AccountManager
    from app.managers.transaction_manager import TransactionManager
    from app.managers.game_manager import GameManager
    from app.managers.ranking_manager import RankingManager

    class __ManagerContainer:
        pass
    app.managers = __ManagerContainer()
    app.managers.account_manager = AccountManager()
    app.managers.transaction_manager = TransactionManager()
    app.managers.game_manager = GameManager()
    app.managers.ranking_manager = RankingManager()
    return app


def _configure_logging(app):
    app.logger.addHandler(logging.getLogger('bulbasaur'))
    app.logger_name = 'bulbasaur'
    return app


def create_app(env):
    init_config(env)
    app = _initialize_flask_app()
    app = _configure_logging(app)
    app.config.from_object(Bulbasaur.Config)
    app = _register_blueprints(app)
    app = _register_version(app)

    db = SQLAlchemy(app)
    mail = Mail(app)
    reg = get_registry()
    reg['DB'] = db
    reg['MAIL'] = mail

    from lib.models.user import User
    from lib.models.role import Role
    from lib.repo.transaction_repo import TransactionRepo
    from lib.repo.user_repo import UserRepo
    from lib.repo.game_repo import GameRepo
    from lib.repo.player_repo import PlayerRepo
    from lib.repo.entry_repo import EntryRepo
    from lib.repo.streak_repo import StreakRepo
    from lib.repo.device_repo import DeviceRepo

    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    Security(app, user_datastore)
    reg['USER_DATASTORE'] = user_datastore

    reg['USER_REPO'] = UserRepo()
    reg['TRANSACTION_REPO'] = TransactionRepo()
    reg['GAME_REPO'] = GameRepo()
    reg['PLAYER_REPO'] = PlayerRepo()
    reg['ENTRY_REPO'] = EntryRepo()
    reg['STREAK_REPO'] = StreakRepo()
    reg['DEVICE_REPO'] = DeviceRepo()

    reg['REDIS_CLIENT'] = redis.StrictRedis(
        host=Bulbasaur.Config.REDIS_HOST,
        port=Bulbasaur.Config.REDIS_PORT,
        db=Bulbasaur.Config.REDIS_DB
    )
    reg['REDIS_LOCK_CLIENT'] = redis.StrictRedis(
        host=Bulbasaur.Config.REDIS_LOCK_HOST,
        port=Bulbasaur.Config.REDIS_LOCK_PORT,
        db=Bulbasaur.Config.REDIS_LOCK_DB
    )

    app = _initialize_managers(app)
    return app

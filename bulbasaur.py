#!/usr/bin/env python
import os
import sys

from app.bulbasaur_app import create_app


bulbasaur_env = os.environ.get('BULBASAUR_ENV')
if not bulbasaur_env:
    print "please set BULBASAUR_ENV environment var"
    sys.exit(1)

application = create_app(bulbasaur_env)

if __name__ == '__main__':
    application.run()

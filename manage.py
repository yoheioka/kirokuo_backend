import os
import sys

from app.bulbasaur_app import create_app
from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand
from lib.registry import get_registry

bulbasaur_env = os.environ.get('BULBASAUR_ENV')
if not bulbasaur_env:
    print "please set BULBASAUR_ENV environment var"
    sys.exit(1)

app = create_app(bulbasaur_env)
db = get_registry()['DB']

from lib.models.role import Role  # NOQA
from lib.models.user import User  # NOQA
from lib.models.player import Player  # NOQA
from lib.models.batter_by_day import BatterByDay  # NOQA
from lib.models.pitcher_by_day import PitcherByDay  # NOQA
from lib.models.game import Game  # NOQA
from lib.models.transaction import Transaction  # NOQA
from lib.models.entry import Entry  # NOQA
from lib.models.streak import Streak  # NOQA
from lib.models.device import Device  # NOQA

migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


@manager.command
def create_roles():
    """Creates the roles."""
    user_datastore = get_registry()['USER_DATASTORE']
    user_datastore.find_or_create_role(name='admin', description='Admin')
    user_datastore.find_or_create_role(name='user', description='User')
    db.session.commit()
    print "successfully created roles"


if __name__ == "__main__":
    manager.run()

# -*- coding: utf-8 -*-
import arrow
import os
import sys

_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_root)

from app.bulbasaur_app import create_app
bulbasaur_env = os.environ.get('BULBASAUR_ENV')
if not bulbasaur_env:
    print 'please set BULBASAUR_ENV environment var'
    sys.exit(1)
create_app(bulbasaur_env)


if __name__ == '__main__':
    from lib.scraping_helpers import get_player_data
    from lib.models.player import Player
    from lib.models.pitcher_by_day import PitcherByDay
    from lib.models.batter_by_day import BatterByDay
    from lib.registry import get_registry
    from lib import redis_lock

    key = __file__
    try:
        with redis_lock.create(key, duration=1200):
            db = get_registry()['DB']
            print '################'
            print 'STARTING SCRAPE NEW PLAYERS'
            print arrow.now('Asia/Tokyo').format(
                'YYYY-MM-DD HH:mm:ss'
            ) + ' JST'

            player_ids = get_player_data.get_player_ids()
            print '%s players found' % len(player_ids)

            # look for players in DB with short name
            player_ids_in_db = [
                player.id for player in Player.query.all() if player.short_name
            ]

            filtered_player_ids = set(
                filter(lambda pid: pid not in player_ids_in_db, player_ids)
            )

            disctinct_pitcher_ids = [
                player.id for player in
                PitcherByDay.query.distinct(PitcherByDay.id).all()
            ]
            for pid in disctinct_pitcher_ids:
                if pid not in player_ids_in_db:
                    filtered_player_ids.add(pid)

            disctinct_batter_ids = [
                player.id
                for player in BatterByDay.query.distinct(BatterByDay.id).all()
            ]
            for pid in disctinct_batter_ids:
                if pid not in player_ids_in_db:
                    filtered_player_ids.add(pid)

            filtered_player_ids = list(filtered_player_ids)

            print '%s players not in db' % len(filtered_player_ids)

            for i, player_id in enumerate(filtered_player_ids):
                if player_id not in player_ids_in_db:
                    player = get_player_data.get_and_save_player_data(
                        player_id)
                    if player:
                        db.session.merge(player)
                        db.session.commit()

                if i > 0 and i % 100 == 0:
                    print '%s players scraped' % i

            print arrow.now('Asia/Tokyo').format(
                'YYYY-MM-DD HH:mm:ss'
            ) + ' JST'
            print 'COMPLETED'
    except redis_lock.LockTimeout:
        print 'Aborting %s. Already running' % key

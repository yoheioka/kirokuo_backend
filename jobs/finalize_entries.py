# -*- coding: utf-8 -*-
import os
import sys

_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_root)

import arrow
from app.bulbasaur_app import create_app
bulbasaur_env = os.environ.get('BULBASAUR_ENV')
if not bulbasaur_env:
    print 'please set BULBASAUR_ENV environment var'
    sys.exit(1)
create_app(bulbasaur_env)


if __name__ == '__main__':
    from lib.entry_finalizer import EntryFinalizer
    from lib import redis_lock

    print '################'
    print 'STARTING FINALIZER'
    print arrow.now('Asia/Tokyo').format('YYYY-MM-DD HH:mm:ss') + ' JST'

    key = __file__
    try:
        with redis_lock.create(key, duration=1200):
            # using utc here should be fine
            date = arrow.utcnow().floor('day')
            print 'date: %s' % date.format('YYYY-MM-DD')
            entry_finalizer = EntryFinalizer(date)
            entry_finalizer.run()

    except redis_lock.LockTimeout:
        print 'Aborting %s. Already running' % key

    print 'COMPLETED'

# -*- coding: utf-8 -*-
import arrow
import os
import sys

_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_root)

from app.bulbasaur_app import create_app
bulbasaur_env = os.environ.get('BULBASAUR_ENV')
if not bulbasaur_env:
    print 'please set BULBASAUR_ENV environment var'
    sys.exit(1)
create_app(bulbasaur_env)


if __name__ == '__main__':
    from lib.scraping_helpers.get_game_data import ScrapeGameHelper
    from lib.registry import get_registry
    from lib import redis_lock

    key = __file__
    try:
        with redis_lock.create(key, duration=60):
            print '################'
            print 'STARTING SCRAPE PLAYER GAME'
            print arrow.now('Asia/Tokyo').format(
                'YYYY-MM-DD HH:mm:ss'
            ) + ' JST'
            db = get_registry()['DB']
            # using utc here should be fine
            date = arrow.utcnow().floor('day')
            scrape_game_helper = ScrapeGameHelper()
            scrape_game_helper.scrape_player_game_data(date)
            print 'COMPLETED'
    except redis_lock.LockTimeout:
        print 'Aborting %s. Already running' % key

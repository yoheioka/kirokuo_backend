# -*- coding: utf-8 -*-
import os
import sys

_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_root)

from app.bulbasaur_app import create_app
bulbasaur_env = os.environ.get('BULBASAUR_ENV')
if not bulbasaur_env:
    print 'please set BULBASAUR_ENV environment var'
    sys.exit(1)
create_app(bulbasaur_env)


if __name__ == '__main__':
    from lib.registry import get_registry
    from lib.push_notifications import PushNotifications

    user_repo = get_registry()['USER_REPO']
    device_repo = get_registry()['DEVICE_REPO']
    user = user_repo.get_user(sys.argv[1])
    device = device_repo.get_latest_device(user)
    device_token = device.device_token

    PushNotifications().send_notification(device_token, sys.argv[2])

# -*- coding: utf-8 -*-
import arrow
import os
import sys

_root = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(_root)

from app.bulbasaur_app import create_app
bulbasaur_env = os.environ.get('BULBASAUR_ENV')
if not bulbasaur_env:
    print 'please set BULBASAUR_ENV environment var'
    sys.exit(1)
create_app(bulbasaur_env)


if __name__ == '__main__':
    from lib.scraping_helpers.get_game_data import ScrapeGameHelper
    from lib.registry import get_registry

    db = get_registry()['DB']
    print '################'
    print 'STARTING SCRAPE SCHEDULE'
    print arrow.now('Asia/Tokyo').format('YYYY-MM-DD HH:mm:ss') + ' JST'

    scrape_game_helper = ScrapeGameHelper()
    games = scrape_game_helper.scrape_game_data(all_dates=True)
    print 'scraping %s games' % len(games)

    for game in games:
        db.session.merge(game)
    db.session.commit()
    print arrow.now('Asia/Tokyo').format('YYYY-MM-DD HH:mm:ss') + ' JST'
    print 'COMPLETED'

"""empty message

Revision ID: 60bdf102d830
Revises: None
Create Date: 2016-03-08 23:56:06.518811

"""

# revision identifiers, used by Alembic.
revision = '60bdf102d830'
down_revision = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.create_table('player',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('num', sa.String(length=3), nullable=True),
    sa.Column('team', sa.String(length=5), nullable=True),
    sa.Column('name', sa.String(length=32), nullable=True),
    sa.Column('position', sa.String(length=5), nullable=True),
    sa.Column('batting', sa.String(length=2), nullable=True),
    sa.Column('throwing', sa.String(length=2), nullable=True),
    sa.Column('img', sa.String(length=255), nullable=True),
    sa.Column('short_name', sa.String(length=32), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('email', sa.String(length=255), nullable=True),
    sa.Column('password', sa.String(length=255), nullable=True),
    sa.Column('active', sa.Boolean(), nullable=True),
    sa.Column('confirmed_at', sa.Integer(), nullable=True),
    sa.Column('source', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email')
    )
    op.create_table('role',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=80), nullable=True),
    sa.Column('description', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('batter_by_day',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('year', sa.Integer(), nullable=False),
    sa.Column('date', sa.Integer(), nullable=False),
    sa.Column('team', sa.String(length=5), nullable=True),
    sa.Column('away', sa.Boolean(), nullable=True),
    sa.Column('starting', sa.Boolean(), nullable=True),
    sa.Column('opponent', sa.String(length=5), nullable=True),
    sa.Column('position', sa.String(length=32), nullable=True),
    sa.Column('ab', sa.Integer(), nullable=True),
    sa.Column('r', sa.Integer(), nullable=True),
    sa.Column('h1b', sa.Integer(), nullable=True),
    sa.Column('h2b', sa.Integer(), nullable=True),
    sa.Column('h3b', sa.Integer(), nullable=True),
    sa.Column('hr', sa.Integer(), nullable=True),
    sa.Column('rbi', sa.Integer(), nullable=True),
    sa.Column('bb', sa.Integer(), nullable=True),
    sa.Column('so', sa.Integer(), nullable=True),
    sa.Column('hbp', sa.Integer(), nullable=True),
    sa.Column('sh', sa.Integer(), nullable=True),
    sa.Column('sf', sa.Integer(), nullable=True),
    sa.Column('sb', sa.Integer(), nullable=True),
    sa.Column('roe', sa.Integer(), nullable=True),
    sa.Column('gdp', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id', 'year', 'date')
    )
    op.create_table('pitcher_by_day',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('year', sa.Integer(), nullable=False),
    sa.Column('date', sa.Integer(), nullable=False),
    sa.Column('team', sa.String(length=5), nullable=True),
    sa.Column('away', sa.Boolean(), nullable=True),
    sa.Column('starting', sa.Boolean(), nullable=True),
    sa.Column('opponent', sa.String(length=5), nullable=True),
    sa.Column('w', sa.Integer(), nullable=True),
    sa.Column('l', sa.Integer(), nullable=True),
    sa.Column('sv', sa.Integer(), nullable=True),
    sa.Column('gp', sa.Integer(), nullable=True),
    sa.Column('ip_times_three', sa.Integer(), nullable=True),
    sa.Column('bf', sa.Integer(), nullable=True),
    sa.Column('h', sa.Integer(), nullable=True),
    sa.Column('hr', sa.Integer(), nullable=True),
    sa.Column('so', sa.Integer(), nullable=True),
    sa.Column('bb', sa.Integer(), nullable=True),
    sa.Column('r', sa.Integer(), nullable=True),
    sa.Column('er', sa.Integer(), nullable=True),
    sa.Column('cg', sa.Integer(), nullable=True),
    sa.Column('cgso', sa.Integer(), nullable=True),
    sa.Column('nh', sa.Integer(), nullable=True),
    sa.Column('pg', sa.Integer(), nullable=True),
    sa.PrimaryKeyConstraint('id', 'year', 'date')
    )
    op.create_table('roles_users',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['role_id'], ['role.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], )
    )
    ### end Alembic commands ###


def downgrade():
    ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('roles_users')
    op.drop_table('pitcher_by_day')
    op.drop_table('batter_by_day')
    op.drop_table('role')
    op.drop_table('user')
    op.drop_table('player')
    ### end Alembic commands ###

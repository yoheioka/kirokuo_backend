#!/bin/bash -e

if [ $1 = "web_app" ] || [ $1 = "worker" ]; then
    DEPLOYENV="bulbasaur"
    if [ $1 = "worker" ]; then
        DEPLOYENV="bulbasaur-worker"
    fi

    echo "removing old .ebextensions"
    rm -rf .ebextensions
    sleep 2
    echo "copying new .ebextensions"
    cp -r aws_extensions/$1/beans .ebextensions
    sleep 2
    echo "init env"
    eb init $DEPLOYENV --profile=bulbasaur
    sleep 2
    echo "starting deploy"
    eb deploy
else
    echo $"Usage: $0 {web_app | worker}"
    exit 1
fi
